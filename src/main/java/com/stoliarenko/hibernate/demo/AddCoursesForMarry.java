package com.stoliarenko.hibernate.demo;

import com.stoliarenko.hibernate.demo.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class AddCoursesForMarry {

    public static void main(String[] args) {
        //create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
        //create session
        Session session = factory.getCurrentSession();
        try{

            //start a transaction
            session.beginTransaction();

            //create a course
            Course tempCourse = new Course("Pacmane - How To Score One Million Points");

            //save the course ... and leverage the cascade all :)
            System.out.println("Saving the course");
            System.out.println("Saved course " + tempCourse);
            session.save(tempCourse);

            //create the student
            Student student = new Student("John", "Doe", "john@luv.com");
            Student student1 = new Student("Mary", "Public", "marry@bv.com");

            //add students to the course
            tempCourse.addStudent(student);
            tempCourse.addStudent(student1);

            //save the student
            session.save(student);
            session.save(student1);
            System.out.println("Saved student: " + tempCourse.getStudents());

            //commit transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        }finally {
            session.close();
            factory.close();
        }
    }
}
