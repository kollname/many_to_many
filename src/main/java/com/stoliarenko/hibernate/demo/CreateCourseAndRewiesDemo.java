package com.stoliarenko.hibernate.demo;

import com.stoliarenko.hibernate.demo.entity.Course;
import com.stoliarenko.hibernate.demo.entity.Instructor;
import com.stoliarenko.hibernate.demo.entity.InstructorDetail;
import com.stoliarenko.hibernate.demo.entity.Review;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateCourseAndRewiesDemo {

    public static void main(String[] args) {
        //create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();
        //create session
        Session session = factory.getCurrentSession();
        try{

            //start a transaction
            session.beginTransaction();

            //create a course
            Course tempCourse = new Course("Pacmane - How To Score One Million Points");

            //add some reviews
            tempCourse.addReview(new Review("Great course ... loved it!"));
            tempCourse.addReview(new Review("Bad course ... loved it!"));
            tempCourse.addReview(new Review("Course??? ... loved it!"));

            //save the course ... and leverage the cascade all :)
            System.out.println("Saving the course");
            System.out.println(tempCourse);
            System.out.println(tempCourse.getReviews());

            session.save(tempCourse);

            //commit transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        }finally {
            session.close();
            factory.close();
        }
    }
}
