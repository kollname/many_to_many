package com.stoliarenko.hibernate.demo;

import com.stoliarenko.hibernate.demo.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateCourseAndStudentsDemo {

    public static void main(String[] args) {
        //create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
        //create session
        Session session = factory.getCurrentSession();
        try{
            //start a transaction
            session.beginTransaction();

            //get the student mary from database
            int studentId = 2;
            Student student = session.get(Student.class, studentId);
            System.out.println("\n Loaded student: " + student);
            System.out.println("Courses: " + student.getCourses());
            //create more corses
            Course course = new Course("Rubik's Cube - How to Speed Cube");
            Course course2 = new Course("Atari 2600 - Game Development");

            //add student to courses
            course.addStudent(student);
            course2.addStudent(student);

            //save the courses
            System.out.println("\n Saving the courses...");
            session.save(course);
            session.save(course2);

            //commit transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        }finally {
            session.close();
            factory.close();
        }
    }
}
